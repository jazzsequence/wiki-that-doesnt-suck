<?php
/**
 * The Template for displaying all single posts.
 *
 * @package wpwtds
 */

get_header(); ?>
<script language="javascript">
function toggle() {
	var ele = document.getElementById("revisions");
	var text = document.getElementById("toggle");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "show revisions";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "hide revisions";
	}
}
</script>
	<?php wpwtds_breadcrumbs(); ?>
	<div id="primary" class="content-area span10">
		<div id="content" class="site-content" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'wiki', 'single' ); ?>

		<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar('wiki'); ?>
<?php get_footer(); ?>