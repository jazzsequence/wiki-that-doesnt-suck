<?php
/**
 * The template for displaying the wiki search form in wpwtds
 *
 * @package wpwtds
 */
?>
	<form method="get" id="searchform" class="searchform row wiki-search" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
		<label for="s" class="span2 wiki-search"><?php _ex( 'Search the Wiki', 'assistive text', 'wpwtds-theme' ); ?></label>
		<input type="search" class="field span8" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" id="s" placeholder="<?php echo esc_attr_x( 'What are you looking for?', 'placeholder', 'wpwtds-theme' ); ?>" />
		<input type="submit" class="btn submit span2" id="searchsubmit" value="<?php echo esc_attr_x( 'Search', 'submit button', 'wpwtds-theme' ); ?>" />
	</form>
