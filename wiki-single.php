<?php
/**
 * @package wpwtds
 */
?>
<?php /* TODO tear this apart */ ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title"><?php the_title(); ?></h1>
		<?php if ( has_post_thumbnail() ) : ?>
			<div class="media">
				<?php the_post_thumbnail( 'article' ); ?>
			</div>
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'wpwtds-theme' ), 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->

	<footer class="entry-meta well">
		<?php
			$term_list = get_the_terms( $post->ID, 'wiki_section' ); // get the taxonomy terms
			if ( count($term_list) > 2 ) { // if there are more than 2 terms, use a comma, e.g.: this, this, that
				$sep = __( ', ', 'wpwtds-theme' );
			} elseif ( count($term_list) === 2 ) { // if there are EXACTLY 2 terms, use an "and", e.g.: this and that
				$sep = __( ' and ', 'wpwtds-theme' );
			} else { // there there is only one (or no) terms, don't use anything
				$sep = null;
			}
			$sep_num = 1; // we'll use this as a counter later
			if ( $term_list ) {
				_e( 'Filed in ', 'wpwtds-theme' );
				foreach ( $term_list as $term ) {
					echo '<a href="' . get_term_link( $term->slug, 'wiki_section' ) . '">' . $term->name . '</a>'; // echo the term
					if ( $sep_num < count($term_list) ) { // if the counter is less than the number of terms, use a seperator
						if ( count($term_list) - $sep_num == 1 ) { // if the difference between the two is down to 1, this is the last term, so use an "and", e.g.: this, this and that
							_e( ' and ', 'wpwtds-theme' );
							$sep_num++;
						} else { // otherwise, keep using the default separator
							echo $sep;
							$sep_num++;
						}
					}
				}
				echo '<br />';
			}
		?>
		<?php wpwtds_posted_on(); ?><br />
		<?php
			echo sprintf( __( 'Bookmark the <a href="%1$s" title="Permalink to %2$s" rel="bookmark">permalink</a>', 'wpwtds-theme' ), get_permalink(), the_title_attribute( 'echo=0' ) );
			edit_post_link( __( 'Edit', 'wpwtds-theme' ), ' | <span class="edit-link">', '</span>' );
			echo '<br />';

			$last_modified = human_time_diff( the_modified_date('U','','', false), current_time('timestamp') );
			$number_of_x = preg_replace('/[^0-9]*/', '', $last_modified);
			if ( strpos( $last_modified, __( 'min', 'wpwtds-theme' ) ) || strpos( $last_modified, __( 'hour', 'wpwtds-theme' ) ) || strpos($last_modified, __( 'sec', 'wpwtds-theme' ) ) ) {
				$wiki_date = $last_modified;
			} elseif (  $number_of_x > 365 ) {
				$wiki_date = __( 'a long time', 'wpwtds-theme' );
			} elseif ( $number_of_x >= 180 ) {
				$wiki_date = __( 'less than a year', 'wpwtds-theme' );
			} elseif ( $number_of_x >= 60 ) {
				$wiki_date = __( 'a couple months', 'wpwtds-theme' );
			} elseif ( $number_of_x >= 30 ) {
				$wiki_date = __( 'a month', 'wpwtds-theme' );
			} elseif ( $number_of_x >= 14 ) {
				$wiki_date = __( 'a couple weeks', 'wpwtds-theme' );
			} elseif ( $number_of_x >= 7 ) {
				$wiki_date = __( 'a week', 'wpwtds-theme' );
			} elseif ( ( $number_of_x <= 3 ) && ( $number_of_x > 1 ) ) {
				$wiki_date = __( 'a couple days', 'wpwtds-theme' );
			} else {
				$wiki_date = $last_modified;
			}
			echo sprintf( __( 'Last updated %1$s ago by %2$s', 'wpwtds-theme' ), $wiki_date, get_the_modified_author() );
			if ( wp_get_post_revisions( $post->ID ) ) {
				echo ' [ <a id="toggle" href="javascript:toggle();">' . __( 'show revisions', 'wpwtds-theme' ) . '</a> ] ';
				echo '<ul id="revisions" style="display: none;">';
				$revisions = wp_get_post_revisions( $post->ID );
				foreach ( $revisions as $revision ) {
					$rev_auth = get_author_name( $revision->post_author );
					$modified = strtotime($revision->post_modified_gmt . ' +0000');
	        		echo '<li id="'. $revision->post_name . '">' . sprintf('%s at %s by %s', date(get_option('date_format'), $modified), date(get_option('time_format'), $modified), $rev_auth) . '</li>';
				}
				echo '</ul>';
			}
		?>
	</footer><!-- .entry-meta -->
</article><!-- #post-## -->
