<?php
/**
 * @package wpwtds
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'wpwtds-theme' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h1>

		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php wpwtds_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php if ( is_search() || is_archive() ) : // Only display Excerpts for Search (or archives) ?>
	<div class="entry-summary">
		<?php if ( has_post_thumbnail() ) { ?>
			<div class="media">
				<a href="<?php the_permalink(); ?>" class="pull-left"><?php the_post_thumbnail('archive'); ?></a>
				<div class="media-body">
					<?php the_excerpt(); ?>
				</div>
			</div>
		<?php } else { ?>
			<?php the_excerpt(); ?>
		<?php } ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<?php if ( has_post_thumbnail() ) { ?>
		<div class="featured-image">
			<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'featured' ); ?></a>
		</div>
	<?php } ?>
	<div class="entry-content">
		<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'wpwtds-theme' ) ); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'wpwtds-theme' ), 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->
	<?php endif; ?>

	<footer class="entry-meta">
		<?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search ?>
			<?php
				/* translators: used between list items, there is a space after the comma */
				$categories_list = get_the_category_list( __( ', ', 'wpwtds-theme' ) );
				if ( $categories_list && wpwtds_categorized_blog() ) :
			?>
			<span class="cat-links">
				<?php printf( __( 'Posted in %1$s', 'wpwtds-theme' ), $categories_list ); ?>
			</span>
			<?php endif; // End if categories ?>

			<?php
				/* translators: used between list items, there is a space after the comma */
				$tags_list = get_the_tag_list( '', __( ', ', 'wpwtds-theme' ) );
				if ( $tags_list ) :
			?>
			<span class="sep"> | </span>
			<span class="tags-links">
				<?php printf( __( 'Tagged %1$s', 'wpwtds-theme' ), $tags_list ); ?>
			</span>
			<?php endif; // End if $tags_list

		/**
		 * if the post is a wiki article
		 */
		elseif ( 'wpwtds_article' == get_post_type() ) :
			$term_list = get_the_terms( $post->ID, 'wiki_section' ); // get the taxonomy terms
			if ( count($term_list) > 2 ) { // if there are more than 2 terms, use a comma, e.g.: this, this, that
				$sep = __( ', ', 'wpwtds-theme' );
			} elseif ( count($term_list) === 2 ) { // if there are EXACTLY 2 terms, use an "and", e.g.: this and that
				$sep = __( ' and ', 'wpwtds-theme' );
			} else { // there there is only one (or no) terms, don't use anything
				$sep = null;
			}
			$sep_num = 1; // we'll use this as a counter later
			?>
			<span class="cat-links">
				<?php
				if ( $term_list ) {
					_e( 'Filed in ', 'wpwtds-theme' );
					foreach ( $term_list as $term ) {
						echo '<a href="' . get_term_link( $term->slug, 'wiki_section' ) . '">' . $term->name . '</a>'; // echo the term
						if ( $sep_num < count($term_list) ) { // if the counter is less than the number of terms, use a seperator
							if ( count($term_list) - $sep_num == 1 ) { // if the difference between the two is down to 1, this is the last term, so use an "and", e.g.: this, this and that
								_e( ' and ', 'wpwtds-theme' );
								$sep_num++;
							} else { // otherwise, keep using the default separator
								echo $sep;
								$sep_num++;
							}
						}
					}
				}
				?>
			</span>
		<?php endif; // End if 'wpwtds_article' == get_post_type() ?>

		<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
		<span class="sep"> | </span>
		<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'wpwtds-theme' ), __( '1 Comment', 'wpwtds-theme' ), __( '% Comments', 'wpwtds-theme' ) ); ?></span>
		<?php endif; ?>

		<?php edit_post_link( __( 'Edit', 'wpwtds-theme' ), '<span class="sep"> | </span><span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-meta -->
</article><!-- #post-## -->
