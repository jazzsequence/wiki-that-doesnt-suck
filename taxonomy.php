<?php
/**
 * The template for displaying WPWTDS taxonomies
 *
 * @package wpwtds
 */

get_header(); ?>
	<?php wpwtds_breadcrumbs(); ?>
	<div id="primary" class="content-area span12">
		<div id="content" class="site-content" role="main">

			<?php if ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'wiki', 'taxonomy' ); ?>

			<?php endif; // end of the loop. ?>
			<!-- TODO add link back to main wiki page
			this will need to be an option set in the theme options -->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>
