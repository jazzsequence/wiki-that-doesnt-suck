<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package wpwtds
 */
?>
	<div id="secondary" class="widget-area span2" role="complementary">
		<?php do_action( 'before_sidebar' ); ?>
		<?php echo do_shortcode('[table_of_contents]'); ?>
		<?php if ( ! dynamic_sidebar( 'wiki-sidebar' ) ) : ?>
		<?php endif; // end sidebar widget area ?>
	</div><!-- #secondary -->