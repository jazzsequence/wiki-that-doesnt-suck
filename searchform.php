<?php
/**
 * The template for displaying search forms in wpwtds
 *
 * @package wpwtds
 */
?>
	<form method="get" id="searchform" class="input-append searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
		<label for="s" class="screen-reader-text"><?php _ex( 'Search', 'assistive text', 'wpwtds-theme' ); ?></label>
		<input type="search" class="field" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" id="s" placeholder="<?php echo esc_attr_x( 'Search this site &hellip;', 'placeholder', 'wpwtds-theme' ); ?>" />
		<input type="submit" class="btn submit" id="searchsubmit" value="<?php echo esc_attr_x( 'Go', 'submit button', 'wpwtds-theme' ); ?>" />
	</form>
