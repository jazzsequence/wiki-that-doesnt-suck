<?php
$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
if ( get_option( 'wiki_main_page' ) ) {
	$wiki_main_uri = get_home_url() . '/?page_id=' . get_option( 'wiki_main_page' );
} else {
	$wiki_main_uri = null;
}
?>
<article id="<?php echo $term->slug; ?>" class="wiki_section <?php echo $term->slug; ?> taxonomy">
	<h1><?php esc_attr_e( $term->name ); ?></h1>

	<section class="row-fluid cat-list">

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="span4 article-link">
				<a class="title" href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
			</div>

		<?php endwhile; ?>

	</section>

	<footer class="entry-meta well">
		<?php echo sprintf( __( 'This is a listing of all the articles in the %s section.', 'wpwtds-theme' ), '<a href="' . home_url() . '/section/' . $term->slug . '/">' . $term->name . '</a>' ); ?><br />
		<?php if ( $wiki_main_uri ) {
			echo sprintf( __( '<a href="%s">Back to Wiki Main Page</a>', 'wpwtds-theme' ), $wiki_main_uri ) . ' &crarr;';
		} ?>
	</footer>
</article>