<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package wpwtds
 */
?>

	</div><!-- #main -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<?php do_action( 'wpwtds_credits' ); ?>
			<a href="http://wordpress.org/" title="<?php esc_attr_e( 'A Semantic Personal Publishing Platform', 'wpwtds-theme' ); ?>" rel="generator"><?php printf( __( 'Proudly powered by %s', 'wpwtds-theme' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>
			<?php printf( __( '%1$s theme by %2$s.', 'wpwtds-theme' ), '<a href="http://museumthemes.com/wiki-theme-url/" target="_blank">Wiki That Doesn\'t Suck</a>', '<a href="http://museumthemes.com/" rel="designer">Museum Themes</a>' ); ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>