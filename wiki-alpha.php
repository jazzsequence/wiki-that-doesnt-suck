<?php
/**
 * The template used for displaying page content in wiki-alpha-page.php
 *
 * @package wpwtds
 */
?>
<?php $wiki_main_uri = get_home_url() . '/?page_id=' . get_option( 'wiki_main_page' ); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php wpwtds_breadcrumbs(); ?>
		<h1 class="entry-title"><?php the_title(); ?></h1>
	</header><!-- .entry-header -->
	<div class="entry-content">
		<?php the_content(); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'wpwtds-theme' ), 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->
	<?php edit_post_link( __( 'Edit', 'wpwtds-theme' ), '<footer class="entry-meta"><span class="edit-link">', '</span></footer>' ); ?>
	<div class="row wiki-sections">
		<section class="span12 alpha-list">
			<?php
			global $paged, $wp_query;
			$temp = $wp_query;
			$post_type = 'wpwtds_article';
			$wiki_query = null;
			$args = array(
				'post_type' => $post_type,
				'post_status' => 'publish',
				'posts_per_page' => 25,
				'paged' => $paged,
				'orderby' => 'title',
				'order' => 'ASC'
			);
			$wp_query = new WP_Query();
			query_posts( $args );
			while ( have_posts() ) : the_post(); ?>
				<div class="span12 article-link">
					<a class="title" href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
				</div>
			<?php endwhile; ?>
		</section>
		<nav class="navigation">
			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-next span6"><?php next_posts_link( __( 'Next page <span class="meta-nav">&rarr;</span>', 'wpwtds-theme' ) ); ?></div>
			<?php endif; ?>

			<?php if ( get_previous_posts_link() ) : ?>
			<div class="nav-previous span6"><?php previous_posts_link( __( '<span class="meta-nav">&larr;</span> Previous page', 'wpwtds-theme' ) ); ?></div>
			<?php endif; ?>
		</nav>
		<?php $wp_query = $temp; ?>
	</div>
	<footer class="entry-meta well">
		<?php echo sprintf( __( 'This is a listing of all wiki articles in alphabetical order.', 'wpwtds-theme' ), '<a href="' . home_url() . '/section/' . $term->slug . '/">' . $term->name . '</a>' ); ?><br />
		<?php if ( $wiki_main_uri ) {
			echo sprintf( __( '<a href="%s">Back to Wiki Main Page</a>', 'wpwtds-theme' ), $wiki_main_uri ) . ' &crarr;';
		} ?>
	</footer>
</article><!-- #post-## -->
