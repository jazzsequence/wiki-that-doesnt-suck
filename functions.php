<?php
/**
 * wpwtds functions and definitions
 *
 * @package wpwtds
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 870; /* pixels */

if ( ! function_exists( 'wpwtds_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function wpwtds_setup() {

	/**
	 * Custom template tags for this theme.
	 */
	require( get_template_directory() . '/inc/template-tags.php' );

	/**
	 * Custom functions that act independently of the theme templates
	 */
	require( get_template_directory() . '/inc/extras.php' );

	/**
	 * Customizer additions
	 */
	require( get_template_directory() . '/inc/customizer.php' );

	/**
	 * smart 404 suggestions
	 */
	include ( get_template_directory() .'/inc/smart_suggestions.php');

	/**
	 * WordPress.com-specific functions and definitions
	 */
	//require( get_template_directory() . '/inc/wpcom.php' );

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on wpwtds, use a find and replace
	 * to change 'wpwtds-theme' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'wpwtds-theme', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'archive', 250, 220, true );
	add_image_size( 'featured', 870, 220, true );
	add_image_size( 'article', 970, 250, true );
	add_image_size( 'full', 1170, 320, true );

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'wpwtds-theme' ),
	) );

	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );
}
endif; // wpwtds_setup
add_action( 'after_setup_theme', 'wpwtds_setup' );

/**
 * Setup the WordPress core custom background feature.
 *
 * Use add_theme_support to register support for WordPress 3.4+
 * as well as provide backward compatibility for WordPress 3.3
 * using feature detection of wp_get_theme() which was introduced
 * in WordPress 3.4.
 *
 * @todo Remove the 3.3 support when WordPress 3.6 is released.
 *
 * Hooks into the after_setup_theme action.
 */
function wpwtds_register_custom_background() {
	$args = array(
		'default-color' => 'ffffff',
		'default-image' => '',
	);

	$args = apply_filters( 'wpwtds_custom_background_args', $args );

	if ( function_exists( 'wp_get_theme' ) ) {
		add_theme_support( 'custom-background', $args );
	} else {
		define( 'BACKGROUND_COLOR', $args['default-color'] );
		if ( ! empty( $args['default-image'] ) )
			define( 'BACKGROUND_IMAGE', $args['default-image'] );
		add_custom_background();
	}
}
add_action( 'after_setup_theme', 'wpwtds_register_custom_background' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function wpwtds_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'wpwtds-theme' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	register_sidebar( array(
		'name'			=> __( 'Wiki Sidebar', 'wpwtds-theme' ),
		'id'			=> 'wiki-sidebar',
		'before_widget'	=> '<aside id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</aside>',
		'before_title'	=> '<h1 class="widget-title">',
		'after_title'	=> '</h1>',
	) );
}
add_action( 'widgets_init', 'wpwtds_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function wpwtds_scripts() {
	wp_enqueue_style( 'wpwtds-style', get_stylesheet_uri() );

	wp_enqueue_script( 'wpwtds-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'wpwtds-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'wpwtds-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
	}
}
add_action( 'wp_enqueue_scripts', 'wpwtds_scripts' );

/**
 * Implement the Custom Header feature
 */
//require( get_template_directory() . '/inc/custom-header.php' );

class _wpwtds_walker_nav_menu extends Walker_Nav_Menu {

	// add classes to ul sub-menus
	function start_lvl( &$output, $depth ) {
	    // depth dependent classes
	    $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
	    $display_depth = ( $depth + 1); // because it counts the first submenu as 0
	    $classes = array(
	        'dropdown-menu',
	        ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
	        ( $display_depth >=2 ? 'sub-sub-menu' : '' ),
	        'menu-depth-' . $display_depth
	        );
	    $class_names = implode( ' ', $classes );

	    // build html
	    $output .= "\n" . $indent . '<ul class="' . $class_names . '" role="menu" aria-labelledby="dLabel">' . "\n";
	}

	// add main/sub classes to li's and links
	 function start_el( &$output, $item, $depth, $args ) {
	    global $wp_query;
	    $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

	    // depth dependent classes
	    $depth_classes = array(
	        ( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
	        ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
	        ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
	        'menu-item-depth-' . $depth
	    );
	    $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

	    // passed classes
	    $classes = empty( $item->classes ) ? array() : (array) $item->classes;
	    $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

	    // build html
	    $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';

	    // link attributes
	    $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
	    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
	    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
	    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
	    $attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '" tabindex="-1"';

	    $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
	        $args->before,
	        $attributes,
	        $args->link_before,
	        apply_filters( 'the_title', $item->title, $item->ID ),
	        $args->link_after,
	        $args->after
	    );

	    // build html
	    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}

/**
 * table of contents shortcode
 */
if ( ! function_exists( 'wpwtds_sc_table_of_contents' ) ) {
	function wpwtds_sc_table_of_contents ( $atts, $content = null ) {
		global $post;
		$defaults = array();

		$atts = shortcode_atts( $defaults, $atts );

		extract( $atts );

		$table_of_contents = wpwtds_get_contents_data( $post->post_content );

		$html = '';
		if ( isset( $table_of_contents['list'] ) && $table_of_contents['list'] != '<ol></ol>' ) {
			$html = '<div class="table_of_contents well">' . '<h4>' . __( 'Contents', 'woothemes' ) . '</h4>' . $table_of_contents['list'] . '</div>' . "\n";
		}

		return apply_filters( 'wpwtds_sc_table_of_contents', $html, $atts );
	} // End wpwtds_sc_table_of_contents()
}

add_shortcode( 'table_of_contents', 'wpwtds_sc_table_of_contents' );

/**
 * contents section
 */
add_filter( 'the_content', 'wpwtds_contents_section_anchors', 10 );

if ( ! function_exists( 'wpwtds_contents_section_anchors' ) ) {
	function wpwtds_contents_section_anchors ( $content ) {
		$data = wpwtds_get_contents_data( $content );

		foreach ( $data['sections_with_ids'] as $k => $v ) {
	  		$content = str_replace( $data['sections'][$k], $v, $content );
	 	}

		return $content;
	} // End wpwtds_contents_section_anchors()
}

/**
 * get contents data
 */
if ( ! function_exists( 'wpwtds_get_contents_data' ) ) {
	function wpwtds_get_contents_data ( $content ) {
	  preg_match_all( "/(<h([0-6]{1})[^<>]*>)([^<>]+)(<\/h[0-6]{1}>)/", $content, $matches, PREG_SET_ORDER );
	  $count = 0; // List Item Count
	  $level = 1; // Heading Level
	  $list = array();
	  $sections = array();
	  $sections_with_ids = array();

	  foreach ( $matches as $val ) {
		$count++;

		if ( $val[2] == $level ) { // If the heading level didn’t change.
			$list[$count] = '<li><a href="#section-' . $count . '">'. $val[3] . '</a>';

		} else if ( $val[2] > $level ) { // If bigger then last heading level, create a nested list.
			$list[$count] = '<ol><li><a href="#section-' . $count . '">'. $val[3] . '</a>';

		} else if ( $val[2] < $level ) { // If less then last Heading Level, end nested list.
			// Account for the number of subheadings, to make sure we indent at the correct level.
			$difference = ( $level - intval( $val[2] ) );
			if ( $difference < 1 ) $difference = 1;

			$closers = '';

			if ( 0 < $difference ) {
				for ( $i = 0; $i < $difference; $i++ ) {
					if ( $i > 0 ) { $closers .= '</ol>'; }
					$closers .= '</li>';
				}
			}

			$list[$count] = $closers . '<li><a href="#section-' . $count . '">'. $val[3] . '</a>';
		}

		$sections[$count] = $val[1] . $val[3] . $val[4]; // Original heading to be Replaced.
		$sections_with_ids[$count] = '<h' . $val[2] . ' id="section-' . $count . '">' . $val[3] . $val[4]; // This is the new Heading.

		$level = $val[2];
	  }

	  switch ( $level ) { // Final markup fix, used if the list ended on a subheading, such as h3, h4. Etc.
	    case 2:
	     $list[$count] .= '</li>';
	    break;
	    case 3:
	     $list[$count] .= '</ol></li>';
	    break;
	    case 4:
	     $list[$count] .= '</ol></li></ol></li>';
	    break;
	    case 5:
	     $list[$count] .= '</ol></li></ol></li></ol></li>';
	    break;
	    case 6:
	     $list[$count] .= '</ol></li></ol></li></ol></li></ol></li></ol></li>';
	    break;
	  }

	  // Setup container to store rendered HTML.
	  $html = '';

	  foreach ( $list as $k => $v ) { // Puts together the list.
	    $html .= $v;
	  }

	  $html = stripslashes( $html );

	  // Add opening and closing <ol> tags only when necessary.
	  if ( substr( $html, 0, 4 ) != '<ol>' ) {
	  	$html = '<ol>' . $html;
	  }

	  if ( substr( $html, -5 ) != '</ol>' ) {
	  	$html .= '</ol>';
	  }

	  return array( 'list' => $html, 'sections' => $sections, 'sections_with_ids' => $sections_with_ids ); // Returns the content
	} // End wpwtds_get_contents_data()
}

function wpwtds_author_archive( &$query ) {
	if ( $query->is_author ){
		$query->set( 'post_type', array( 'wpwtds_article', 'post', 'page' ) );
	}
}
add_action( 'pre_get_posts', 'wpwtds_author_archive' );