<?php
/*
Plugin Name: Smart Suggestions
Description: Displays a list of smart suggestions for 404 pages based on the URL the user entered (or went to).
Version: 1.0
Author: Chris Reynolds
Author URI: http://www.arcanepalette.com
*/

/*  Copyright 2012 Chris Reynolds <chris@jazzsequence.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/*	Notes:
	This is a fork of Mike Tyson's Smart 404 Redirect plugin.  It was built
	to be redistributed as part of the Opal E-commerce theme, but can be used
	as a standalone plugin.  No support is offered (or will be given) for any
	use other than as it was intended for this theme (e.g. as a standalone
	plugin).  For any questions regarding the function or use of this plugin,
	please refer to the Smart 404 Redirect support forums.
	http://wordpress.org/support/plugin/smart-404
*/

/**
 * Main action handler
 *
 * @package Smart Suggestions
 * @since 0.1
 *
 * Searches through posts to see if any matches the REQUEST_URI.
 * Also searches tags
 */
function ap_smartsugg_redirect() {
	global $ap_404;
	if ( !is_404() )
		return;
	
	// Extract any GET parameters from URL
	$get_params = "";
	if ( preg_match("@/?(\?.*)@", $_SERVER["REQUEST_URI"], $matches) ) {
	    $get_params = $matches[1];
	}
	
	// Extract search term from URL
	$patterns_array = array();
	if ( ( $patterns = trim( get_option('ignored_patterns' ) ) ) ) {
		$patterns_array = explode( '\n', $patterns );
	}
	
	$patterns_array[] = "/(trackback|feed|(comment-)?page-?[0-9]*)/?$";
	$patterns_array[] = "\.(html|php)$";
	$patterns_array[] = "/?\?.*";
	$patterns_array = array_map(create_function('$a', '$sep = (strpos($a, "@") === false ? "@" : "%"); return $sep.trim($a).$sep."i";'), $patterns_array);
	
	$search = preg_replace( $patterns_array, "", urldecode( $_SERVER["REQUEST_URI"] ) );
	$search = basename(trim($search));
	$search = str_replace("_", "-", $search);
	$search = trim(preg_replace( $patterns_array, "", $search));
	
	if ( !$search ) return;
	
	$search_words = trim(preg_replace( "@[_-]@", " ", $search));
	$GLOBALS["__ap_smartsugg"]["search_words"] = explode(" ", $search_words);
    $GLOBALS["__ap_smartsugg"]["suggestions"] = array();

    $search_groups = (array)get_option( 'also_search' );
    if ( !$search_groups ) $search_groups = array("posts","pages","tags","categories","topic","ap_products","forum");
    
    // Now perform general search
    foreach ( $search_groups as $group ) {
        switch ( $group ) {
            case "posts":
                $posts = ap_smartsugg_search($search, "post");
           		$GLOBALS["__ap_smartsugg"]["suggestions"] = array_merge ( (array)$GLOBALS["__ap_smartsugg"]["suggestions"], $posts );
                break;
            case "wpwtds_article":
                $posts = ap_smartsugg_search($search, "wpwtds_article");
           		$GLOBALS["__ap_smartsugg"]["suggestions"] = array_merge ( (array)$GLOBALS["__ap_smartsugg"]["suggestions"], $posts );
                break;
            case "pages":
                $posts = ap_smartsugg_search($search, "page");
           		$GLOBALS["__ap_smartsugg"]["suggestions"] = array_merge ( (array)$GLOBALS["__ap_smartsugg"]["suggestions"], $posts );
        		break;
        }
    }
}


/**
 * Helper function for searching
 *
 * @package Smart Suggestions
 * @since 0.5
 * @param   query   Search query
 * @param   type    Entity type (page or post)
 * @return  Array of results
 */
function ap_smartsugg_search($search, $type) {
    $search_words = trim(preg_replace( "@[_-]@", " ", $search));
	$posts = get_posts( array( "s" => $search_words, "post_type" => $type ) );
	if ( count( $posts ) > 1 ) {
	    // See if search phrase exists in title, and prioritise any single match
	    $titlematches = array();
	    foreach ( $posts as $post ) {
	        if ( strpos(strtolower($post->post_title), strtolower($search_words)) !== false ) {
	            $titlematches[] = $post;
	        }
	    }
	    if ( count($titlematches) == 1 ) {
	        return $titlematches;
	    }
	}
	
	return $posts;
}
 
 
/**
 * Filter to keep the inbuilt 404 handlers at bay
 *
 * @package Smart Suggestions
 * @since 0.3
 *
 */
function ap_smartsugg_redirect_canonical_filter($redirect, $request) {
	
	if ( is_404() ) {
		// 404s are our domain now - keep redirect_canonical out of it!
		return false;
	}
	
	// redirect_canonical is good to go
	return $redirect;
}


/**
 * Template tag to determine if there any suggested posts
 *
 * @package Smart Suggestions
 * @since 0.1
 *
 * @return	boolean	True if there are some suggestions, false otherwise
 */
function ap_smartsugg_has_suggestions() {
	return ( isset ( $GLOBALS["__ap_smartsugg"]["suggestions"] ) && is_array( $GLOBALS["__ap_smartsugg"]["suggestions"] ) && count( $GLOBALS["__ap_smartsugg"]["suggestions"] ) > 0 ); 
}

/**
 * Template tag to obtain suggested posts
 *
 * @package Smart Suggestions
 * @since 0.1
 *
 * @return	array	Array of posts
 */
function ap_smartsugg_get_suggestions() {
	return $GLOBALS["__ap_smartsugg"]["suggestions"];
}

/**
 * Template tag to render HTML list of suggestions
 *
 * @package Smart Suggestions
 * @since 0.1
 *
 * @param	format	string	How to display the items: flat (just links, separated by line-breaks), list (li items)
 * @return	boolean	True if some suggestions were rendered, false otherwise
 */
function ap_smartsugg_suggestions($format = 'flat') {
	if ( !isset ( $GLOBALS["__ap_smartsugg"]["suggestions"] ) || !is_array( $GLOBALS["__ap_smartsugg"]["suggestions"] ) || count( $GLOBALS["__ap_smartsugg"]["suggestions"] ) == 0 ) 
		return false;
	
	echo '<div id="ap_smartsugg_suggestions">';
	if ( $format == 'list' )
		echo '<ul>';
		
	foreach ( (array) $GLOBALS["__ap_smartsugg"]["suggestions"] as $post ) {
		if ( $format == "list" )
			echo '<li>';
			
		?>
		<a href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a>
		<?php
		
		if ( $format == "list" )
			echo '</li>';
		else if ( $format == "flat" )
			echo '<br />';
	}
	
	if ( $format == 'list ')
		echo '</ul>';
		
	echo '</div>';
	
	return true;
}

/**
 * Template tag to initiate 'The Loop' with suggested posts
 *
 * @package Smart Suggestions
 * @since 0.1
 *
 * @return	boolean	True if there are some posts to loop over, false otherwise
 */
function ap_smartsugg_loop() {
	if ( !isset ( $GLOBALS["__ap_smartsugg"]["suggestions"] ) || !is_array( $GLOBALS["__ap_smartsugg"]["suggestions"] ) || count( $GLOBALS["__ap_smartsugg"]["suggestions"] ) == 0 ) {
		return false;
	}
	
	$postids = array_map(create_function('$a', 'return $a->ID;'), $GLOBALS["__ap_smartsugg"]["suggestions"]);
	
	query_posts( array( "post__in" => $postids ) );
	return have_posts();
}

/**
 * Template tag to retrieve array of search terms used
 *
 * @package Smart 404
 * @since 0.4
 *
 * @return Array of search terms
 */
function ap_smartsugg_get_search_terms() {
    return $GLOBALS["__ap_smartsugg"]["search_words"];
}

// Set up plugin

add_action( 'template_redirect', 'ap_smartsugg_redirect' );
add_filter( 'redirect_canonical', 'ap_smartsugg_redirect_canonical_filter', 10, 2 );
add_option( 'also_search', array( 'posts', 'pages', 'tags', 'categories', 'wpwtds_article' ) );
add_option( 'ignored_patterns', '' );

?>
