<?php
/**
 * wpwtds Theme Customizer
 *
 * @package wpwtds
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function wpwtds_customize_register( $wp_customize ) {

	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	$wp_customize->add_section( 'wpwtds_wiki_options', array( 'title' => __( 'Wiki Options', 'wpwtds-theme' ) ) );
	$wp_customize->add_setting( 'wiki_main_page', array(
		'default' => get_option('wiki_main_page'),
	) );
	$wp_customize->add_control( 'wiki_main_page', array(
		'type' => 'dropdown-pages',
		'label' => __( 'Wiki Main Page', 'wpwtds-theme' ),
		'section' => 'wpwtds_wiki_options'
	) );
	$wp_customize->add_setting( 'wiki_featured_tax', array(
		'default' => 0,
	) );
	$wp_customize->add_control( 'wiki_featured_tax', array(
		'label' => __( 'Featured Wiki Section', 'wpwtds-theme' ),
		'section' => 'wpwtds_wiki_options',
		'settings' => 'wiki_featured_tax',
		'type' => 'select',
		'choices' => array_merge( array( __( 'None', 'wpwtds-theme' ) ), get_terms( 'wiki_section', array( 'orderby' => 'name', 'fields' => 'names' ) ) )
	) );
	$wp_customize->add_setting( 'wiki_secondary_tax', array(
		'default' => 0,
	) );
	$wp_customize->add_control( 'wiki_secondary_tax', array(
		'label' => __( 'Secondary Wiki Section', 'wpwtds-theme' ),
		'section' => 'wpwtds_wiki_options',
		'settings' => 'wiki_secondary_tax',
		'type' => 'select',
		'choices' => array_merge( array( __( 'None', 'wpwtds-theme' ) ), get_terms( 'wiki_section', array( 'orderby' => 'name', 'fields' => 'names' ) ) )
	) );

}
add_action( 'customize_register', 'wpwtds_customize_register' );


/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function wpwtds_customize_preview_js() {
	wp_enqueue_script( 'wpwtds_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130304', true );
}
add_action( 'customize_preview_init', 'wpwtds_customize_preview_js' );
