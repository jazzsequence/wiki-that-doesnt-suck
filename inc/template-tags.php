<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package wpwtds
 */

if ( ! function_exists( 'wpwtds_content_nav' ) ) :
/**
 * Display navigation to next/previous pages when applicable
 */
function wpwtds_content_nav( $nav_id ) {
	global $wp_query, $post;

	// Don't print empty markup on single pages if there's nowhere to navigate.
	if ( is_single() ) {
		$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
		$next = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous )
			return;
	}

	// Don't print empty markup in archives if there's only one page.
	if ( $wp_query->max_num_pages < 2 && ( is_home() || is_archive() || is_search() ) )
		return;

	$nav_class = ( is_single() ) ? 'navigation-post' : 'navigation-paging';

	?>
	<nav role="navigation" id="<?php echo esc_attr( $nav_id ); ?>" class="<?php echo $nav_class; ?> row-fluid">

	<?php if ( is_single() ) : // navigation links for single posts ?>

		<?php previous_post_link( '<div class="nav-previous span6">%link</div>', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'wpwtds-theme' ) . '</span> %title' ); ?>
		<?php next_post_link( '<div class="nav-next span6">%link</div>', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'wpwtds-theme' ) . '</span>' ); ?>

	<?php elseif ( $wp_query->max_num_pages > 1 && ( is_home() || is_archive() || is_search() ) ) : // navigation links for home, archive, and search pages ?>

		<?php if ( get_next_posts_link() ) : ?>
		<div class="nav-previous span6"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'wpwtds-theme' ) ); ?></div>
		<?php endif; ?>

		<?php if ( get_previous_posts_link() ) : ?>
		<div class="nav-next span6"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'wpwtds-theme' ) ); ?></div>
		<?php endif; ?>

	<?php endif; ?>

	</nav><!-- #<?php echo esc_html( $nav_id ); ?> -->
	<?php
}
endif; // wpwtds_content_nav

if ( ! function_exists( 'wpwtds_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 */
function wpwtds_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'wpwtds-theme' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'wpwtds-theme' ), '<span class="edit-link">', '<span>' ); ?></p>
	<?php
			break;
		default :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment">
			<footer>
				<div class="comment-author vcard">
					<?php echo get_avatar( $comment, 40 ); ?>
					<?php printf( __( '%s <span class="says">says:</span>', 'wpwtds-theme' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
				</div><!-- .comment-author .vcard -->
				<?php if ( $comment->comment_approved == '0' ) : ?>
					<em><?php _e( 'Your comment is awaiting moderation.', 'wpwtds-theme' ); ?></em>
					<br />
				<?php endif; ?>

				<div class="comment-meta commentmetadata">
					<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>"><time datetime="<?php comment_time( 'c' ); ?>">
					<?php printf( _x( '%1$s at %2$s', '1: date, 2: time', 'wpwtds-theme' ), get_comment_date(), get_comment_time() ); ?>
					</time></a>
					<?php edit_comment_link( __( 'Edit', 'wpwtds-theme' ), '<span class="edit-link">', '<span>' ); ?>
				</div><!-- .comment-meta .commentmetadata -->
			</footer>

			<div class="comment-content"><?php comment_text(); ?></div>

			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			</div><!-- .reply -->
		</article><!-- #comment-## -->

	<?php
			break;
	endswitch;
}
endif; // ends check for wpwtds_comment()

if ( ! function_exists( 'wpwtds_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function wpwtds_posted_on() {
	printf( __( 'Posted on <a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a><span class="byline"> by <span class="author vcard"><a class="url fn n" href="%5$s" title="%6$s" rel="author">%7$s</a></span></span>', 'wpwtds-theme' ),
		esc_url( get_permalink() ),
		esc_attr( get_the_time() ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
		esc_attr( sprintf( __( 'View all posts by %s', 'wpwtds-theme' ), get_the_author() ) ),
		get_the_author()
	);
}
endif;
/**
 * Returns true if a blog has more than 1 category
 */
function wpwtds_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'all_the_cool_cats' ) ) ) {
		// Create an array of all the categories that are attached to posts
		$all_the_cool_cats = get_categories( array(
			'hide_empty' => 1,
		) );

		// Count the number of categories that are attached to the posts
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'all_the_cool_cats', $all_the_cool_cats );
	}

	if ( '1' != $all_the_cool_cats ) {
		// This blog has more than 1 category so wpwtds_categorized_blog should return true
		return true;
	} else {
		// This blog has only 1 category so wpwtds_categorized_blog should return false
		return false;
	}
}

/**
 * Flush out the transients used in wpwtds_categorized_blog
 */
function wpwtds_category_transient_flusher() {
	// Like, beat it. Dig?
	delete_transient( 'all_the_cool_cats' );
}
add_action( 'edit_category', 'wpwtds_category_transient_flusher' );
add_action( 'save_post', 'wpwtds_category_transient_flusher' );

function wpwtds_breadcrumbs() {
	/* this sets up some breadcrumbs for posts & pages that support Twitter Bootstrap styles */
	global $wp_query, $post;

	/**
	 * check if this is an archive page and if so, if it's the first page
	 */
	$paged = $wp_query->get( 'paged' );
	if ( !$paged || $paged < 2 ) :
		$first_page = true;
	else :
		$first_page = false;
	endif;

	$separator = ' <span class="divider">&rsaquo;</span>'; // this is the divider
	if ( get_option( 'wiki_main_page' ) ) {
		$wiki_main_uri = get_home_url() . '/?page_id=' . get_option( 'wiki_main_page' );
	} else {
		$wiki_main_uri = null;
	}

	/**
	 * this deals with paged results, if we're looking at an archive of some kind
	 * if it's not an archive page, it just displays the page title
	 * @param $separator (required) - this is the divider. we need this
	 * @param $paged (required) - the paged variable, e.g. whether the page is paginated or not
	 * @param $first_page (required) - whether we're on the first page or not
	 * @param $title (optional) - a title to display in the breadcrumb. will be set to the_title if not specified
	 */
	function do_paged($separator, $paged, $first_page, $title = null) {
		if ( !$title )
			$title = get_the_title();

		if ( !$first_page ) {
			if ( is_ssl() ) {
				$current_page = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			} else {
				$current_page = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			}
			$origin_page = preg_replace( '.page/\d/.', '', $current_page);

			echo '<li><span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="' . $origin_page . '" title="' . get_the_title() . '">' . $title . '</a></span>' . $separator . '</li>';
			echo '<li class="active">' . sprintf( __( 'Page %s', 'wpwtds-theme' ), $paged ) . '</li>';
		} else {
			echo '<li class="active">' . $title . '</li>';
		}
	}

	/**
	 * start echoing the breadcrumbs
	 */
	echo '<ul xmlns:v="http://rdf.data-vocabulary.org/#" class="breadcrumb">';
	echo '<li><small>' . __( 'You are here:' ) . '</small><span class="divider"></span></li>';

	/**
	 * if this is the front page
	 */
	if ( is_front_page() ) {
		echo '<li class="active">' . get_the_title() . '</li>';
	} else {
		echo '<li><span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="' . get_home_url() . '">' . __( 'Home', 'wpwtds-theme' ) . '</a></span>' . $separator . '</li>';

		/**
		 * if this is a page and if it has a parent page
		 */
		if ( is_page() && $post->post_parent ) {
			/* get the parent page breadcrumb */
			$parent_title = get_the_title($post->post_parent);
			if ( $parent_title != the_title(' ', ' ', false) ) {
				echo '<li><span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href=' . get_permalink($post->post_parent) . ' ' . 'title=' . $parent_title . '>' . $parent_title . '</a></span>' . $separator . '</li>';
				do_paged($separator, $paged, $first_page);
			}
		}

		/**
		 * if this is a page but DOESN'T have a parent page
		 */
		elseif ( is_page() && !$post->post_parent ) {
			do_paged($separator, $paged, $first_page);
		}

		/**
		 * if this is a search page
		 */
		elseif ( is_search() ) {
			do_paged($separator, $paged, $first_page, __('Search results', 'wpwtds-theme'));
		}

		/**
		 * if this is a 404 page
		 */
		elseif ( is_404() ) {
			/* we don't use do_paged here because a 404 page isn't going to be paginated */
			echo '<li class="active">' . __( 'Not found', 'wpwtds-theme' ) . '</li>';
		}

		/**
		 * if this is the blog page
		 */
		elseif ( is_home() ) {
			do_paged($separator, $paged, $first_page, __('Blog', 'wpwtds-theme'));
		}

		/**
		 * if this is an archive page
		 */
		elseif ( is_archive() ) {
			/* is this a tag archive? */
			if ( is_tag() ) {
				$title = single_tag_title( '', false );
			}
			/* is this a category archive? */
			elseif ( is_category() ) {
				$title = single_cat_title('',false);
			}
			/* is this an author archive? (not bothering with the author name here) */
			elseif ( is_author() ) {
				$author = $wp_query->queried_object;
				$title = sprintf( __( 'Author archive for %s', 'wpwtds-theme' ), $author->display_name );
			}
			/* is this a single day archive? */
			elseif ( is_day() ) {
				$title = get_the_date();
			}
			/* is this a month archive? */
			elseif ( is_month() ) {
				$title = get_the_date( 'F Y' );
			}
			/* is this a year archive? */
			elseif ( is_year() ) {
				$title = get_the_date( 'Y' );
			}
			elseif ( is_tax() ) {
				$tax = $wp_query->queried_object;
				if ( $wiki_main_uri ) {
					echo '<li><span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="' . $wiki_main_uri . '">' . __( 'Wiki Main Page', 'wpwtds-theme' ) . '</a></span>' . $separator . '</li>';
				}
				$title = $tax->name;
			}
			do_paged($separator, $paged, $first_page, $title);
		}

		/**
		 * if this is a wiki article
		 */
		elseif ( 'wpwtds_article' == get_post_type() ) {
			if ( $wiki_main_uri ) {
				echo '<li><span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="' . $wiki_main_uri . '">' . __( 'Wiki Main Page', 'wpwtds-theme' ) . '</a></span>' . $separator . '</li>';
			}
			$terms = get_the_terms( $post->ID, 'wiki_section' );
			if ( $terms && ! is_wp_error( $terms ) ) {
				foreach ( $terms as $term ) {
					echo '<li><span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="' . get_term_link($term->slug, 'wiki_section') . '">' . $term->name . '</a></span>' . $separator . '</li>';
				}
			}
			do_paged($separator, $paged, $first_page);

		}

		/**
		 * for all other requests
		 */
		else {
			// first, display the blog page link, since that's a global parent, but only if it's set to be different than the home page
			if ( get_option('page_for_posts') ) {
				// defines the blog page if it's set
				$blog_page_uri = get_permalink( get_option( 'page_for_posts' ) );
				echo '<li><span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="' . $blog_page_uri . '">' . __( 'Blog', 'wpwtds-theme' ) . '</a></span>' . $separator . '</li>';
			}
			// this is a post, so get the category, if it exists
			$category = get_the_category();
			if ($category) {
				foreach($category as $category) {
				echo '<li><span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="' . get_category_link($category->term_id) . '">' . $category->name . '</a></span>' . $separator . '</li>';
				}
			}
			do_paged($separator, $paged, $first_page);
		}
	}
	echo '</ul>';
}

/**
 * get tax rel
 * this function (ingeniously) recreates the array used by the wp_customizer for the taxonomies so we can then
 * associate the right taxonomy id with the selection made in the customizer
 * this is a workaround because we can't give the 'select' type option specific values, and because it wasn't saving
 * when I tried to create my own dropdown
 * @param $value -- the value you want to check for, e.g. the theme_mod value
 * @return $your_tax_is -- the id of the taxonomy you're checking
 */
function wpwtds_get_tax_rel( $value ) {
	$terms = array_merge( array( __( 'None', 'wpwtds-theme' ) ), get_terms( 'wiki_section', array( 'orderby' => 'name', 'fields' => 'ids' ) ) );
	$your_tax_is = $terms[$value];
	return $your_tax_is;
}