<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package wpwtds
 */

get_header(); ?>

	<div id="primary" class="span12 content-area">
		<div id="content" class="site-content" role="main">

		<?php
		/**
		 * First, let's see what was queried
		 */
		$term = stripcslashes( $_SERVER['REDIRECT_URL'] );
		$term = str_replace( "-", " ", $term );
		$term = str_replace( "/", " ", $term );
		?>

			<article id="post-0" class="post error404 not-found">
				<header class="entry-header">
					<?php wpwtds_breadcrumbs(); ?>
					<h1 class="entry-title"><?php echo sprintf( __( 'Whoops! We could not find &ldquo; %s &rdquo;', 'wpwtds-theme' ), $term ); ?></h1>
				</header><!-- .entry-header -->

				<div class="entry-content">
					<p><?php echo sprintf( __( 'There was no article found for &ldquo; %s &rdquo;.', 'wpwtds-theme' ), $term); ?></p>

					<?php
					if ( ap_smartsugg_has_suggestions() ) {
						echo '<p>';
						_e( 'Were you looking for one of these articles?', 'wpwtds-theme' );
						echo '</p>';
						ap_smartsugg_suggestions();
						$suggestions = 1;
					}
					?>

					<aside class="four-oh-four-search">
						<?php get_search_form(); ?>
					</aside>

					<!-- TODO add a back to main link here, that links back to the wiki main page -->

					<section class="row-fluid">
						<div class="span3">
							<?php the_widget( 'WP_Widget_Recent_Posts' ); ?>
						</div>

						<?php if ( wpwtds_categorized_blog() ) : // Only show the widget if site has multiple categories. ?>
						<div class="span3 widget widget_categories">
							<h2 class="widgettitle"><?php _e( 'Most Used Categories', 'wpwtds-theme' ); ?></h2>
							<ul>
								<?php wp_list_categories( array( 'orderby' => 'count', 'order' => 'DESC', 'show_count' => 1, 'title_li' => '', 'number' => 10 ) ); ?>
							</ul>
						</div><!-- .widget -->
						<?php endif; ?>

						<div class="span3">
							<?php
							/* translators: %1$s: smiley */
							$archive_content = '<p>' . __( 'Try looking in the monthly archives.', 'wpwtds-theme' ) . '</p>';
							the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content" );
							?>
						</div>

						<div class="span3">
							<?php the_widget( 'WP_Widget_Tag_Cloud' ); ?>
						</div>
					</section>

				</div><!-- .entry-content -->
			</article><!-- #post-0 .post .error404 .not-found -->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>