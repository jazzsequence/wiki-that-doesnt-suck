<?php
/**
 * The template used for displaying page content in wiki-main-page.php
 *
 * @package wpwtds
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php wpwtds_breadcrumbs(); ?>
		<h1 class="entry-title"><?php the_title(); ?></h1>
	</header><!-- .entry-header -->
	<aside class="search">
		<?php get_template_part( 'wiki', 'searchform' ); ?>
	</aside>
	<div class="entry-content">
		<?php the_content(); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'wpwtds-theme' ), 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->
	<?php edit_post_link( __( 'Edit', 'wpwtds-theme' ), '<footer class="entry-meta"><span class="edit-link">', '</span></footer>' ); ?>
		<?php
		$post_type = 'wpwtds_article';
		$tax = 'wiki_section';
		$tax_terms = get_terms( $tax, 'orderby=count&order=DESC' );
		$featured_tax = wpwtds_get_tax_rel( get_theme_mod( 'wiki_featured_tax' ) );
		$secondary_tax = wpwtds_get_tax_rel( get_theme_mod( 'wiki_secondary_tax' ) );
		$wiki_query = null;
		if ( $tax_terms ) {
			foreach ( $tax_terms as $tax_term ) {
					$args = array(
						'post_type' => $post_type,
						"$tax" => $tax_term->name,
						'post_status' => 'publish',
						'posts_per_page' => -1,
						'caller_get_posts' => -1,
						'orderby' => 'title',
						'order' => 'ASC',
					);
					$wiki_query = new WP_Query( $args );

					if ( $tax_term->term_id == $featured_tax ) :
						if ( $wiki_query->have_posts() ) :
						ob_start(); ?>
						<section class="well featured-tax <?php echo $tax_term->slug; ?>">
							<legend><?php _e( 'Featured topics', 'wpwtds-theme' ); ?></legend>
							<h3><a href="<?php echo get_term_link($tax_term->slug, 'wiki_section'); ?>" title="<?php esc_attr_e( $tax_term->name ); ?>"><?php esc_attr_e( $tax_term->name ); ?></a></h3>
							<div class="row-fluid">
								<?php while ( $wiki_query->have_posts() ) : $wiki_query->the_post(); ?>
								<div class="span6 article-link">
									<a class="title" href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
								</div>
								<?php endwhile; ?>
							</div>
						</section>
						<?php endif;

					$featured_ob = ob_get_contents();
					ob_end_clean();

					elseif ( $tax_term->term_id == $secondary_tax ) :
						if ( $wiki_query->have_posts() ) :
						ob_start(); ?>
						<section class="span12 secondary-tax <?php echo $tax_term->slug; ?>">
							<legend><?php _e( 'Other topics', 'wpwtds-theme' ); ?></legend>
							<h3><a href="<?php echo get_term_link($tax_term->slug, 'wiki_section'); ?>" title="<?php esc_attr_e( $tax_term->name ); ?>"><?php esc_attr_e( $tax_term->name ); ?></a></h3>
							<div class="row-fluid">
								<?php while ( $wiki_query->have_posts() ) : $wiki_query->the_post(); ?>
								<div class="span6 article-link">
									<a class="title" href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
								</div>
								<?php endwhile; ?>
							</div>
						</section>
						<?php endif;

					$secondary_ob = ob_get_contents();
					ob_end_clean();

					endif;
				}

				if ( $featured_ob ) : echo $featured_ob; endif;
				if ($secondary_ob ) :
					echo $secondary_ob;
					echo '<hr class="row" />';
				endif;
				echo '<div class="row wiki-sections">';
				foreach ( $tax_terms as $tax_term ) {
					$args = array(
						'post_type' => $post_type,
						"$tax" => $tax_term->name,
						'post_status' => 'publish',
						'posts_per_page' => -1,
						'caller_get_posts' => -1,
						'orderby' => 'title',
						'order' => 'ASC',
					);
					$wiki_query = new WP_Query( $args );
					if ( $tax_term->term_id != $featured_tax && $tax_term->term_id != $secondary_tax ) :
						echo '<div class="other-topics">';
						if ( $wiki_query->have_posts() ) : ?>
							<section class="span6 <?php echo $tax_term->slug; ?>">
								<h3><a href="<?php echo get_term_link($tax_term->slug,'wiki_section'); ?>" title="<?php esc_attr_e( $tax_term->name ); ?>"><?php esc_attr_e( $tax_term->name ); ?></a></h3>
								<?php while ( $wiki_query->have_posts() ) : $wiki_query->the_post(); ?>
								<div class="span6 article-link">
									<a class="title" href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
								</div>
								<?php endwhile; ?>
							</section>
						<?php endif;
						echo '</div>';
					endif;
				}
			}

		?>
	</div>
</article><!-- #post-## -->
