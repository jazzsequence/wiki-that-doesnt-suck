<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package wpwtds
 */

get_header();
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$author = get_queried_object();
$author_bio = get_the_author_meta( 'description', $author->ID );
$author_url = get_the_author_meta( 'user_url', $author->ID );
?>

	<?php wpwtds_breadcrumbs(); ?>
	<div id="primary" class="content-area span12">
		<div id="content" class="site-content" role="main">

			<div class="hero-unit media">
				<div class="media-object pull-left">
					<?php echo get_avatar( $author->ID, 150, '', $author->display_name ); ?>
				</div>
				<div class="media-body">
					<h2 class="media-heading"><?php echo $author->display_name; ?></h2>
					<?php if ( $author_url ) { ?>
						<a href="<?php echo $author_url; ?>" target="_blank"><?php echo $author_url; ?></a>
					<?php } ?>
					<?php if ( $author_bio ) {
						echo wpautop( $author_bio );
					} ?>
				</div>
			</div>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'author' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || '0' != get_comments_number() )
						comments_template();
				?>

			<?php endwhile; // end of the loop. ?>
		<nav class="navigation row">
			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-next span6"><?php next_posts_link( __( 'Next page <span class="meta-nav">&rarr;</span>', 'wpwtds-theme' ) ); ?></div>
			<?php endif; ?>

			<?php if ( get_previous_posts_link() ) : ?>
			<div class="nav-previous span6"><?php previous_posts_link( __( '<span class="meta-nav">&larr;</span> Previous page', 'wpwtds-theme' ) ); ?></div>
			<?php endif; ?>
		</nav>
		<footer class="entry-meta well">
			<?php echo sprintf( __( 'This is a listing of all the posts and articles written by %s.', 'wpwtds-theme' ), $author->display_name ); ?><br />
		</footer>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>