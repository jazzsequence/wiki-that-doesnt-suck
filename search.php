<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package wpwtds
 */

get_header(); ?>

	<section id="primary" class="content-area span12">
		<div id="content" class="site-content" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php wpwtds_breadcrumbs(); ?>
				<h1 class="page-title"><?php printf( __( 'Search Results for &ldquo;%s&rdquo;', 'wpwtds-theme' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'search' ); ?>

			<?php endwhile; ?>

			<?php wpwtds_content_nav( 'nav-below' ); ?>

		<?php else : ?>

			<?php get_template_part( 'no-results', 'search' ); ?>

		<?php endif; ?>

		</div><!-- #content -->
	</section><!-- #primary -->

<?php get_footer(); ?>